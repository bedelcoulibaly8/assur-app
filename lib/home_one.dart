import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:date_field/date_field.dart';
import 'package:gnaassurance/home_two.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';








class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}


enum SingingCharacter { un_ans, six_mois,trois_mois,un_mois }
enum SingingCharacters { auto,moto }
enum SingingCharacters_model { auto_break,monospace,citadine,cabriole,pic_up,quatre_quartre,crossovers}
enum SingingCharacters_categorie { tourisme, prope_compte}

enum SingingCharacters_ { essence,gasoil}

class _HomeState extends State<Home> {


  _Alert_controller(context) {
    ProgressDialog pr = ProgressDialog(context,type: ProgressDialogType.Normal,isDismissible: true);
    Alert(
      context: context,
      title: "ERROR",
      style:alertStyle,
      desc: "Il y a des champs vide",
      buttons: [
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.black, fontSize: 20),

          ),
          //  onPressed: () => Navigator.pop(context),
          onPressed:(){
            Navigator.push(context,
              MaterialPageRoute(builder: (context) => Home()),
            );
          } ,
          width: 120,
          color: Colors.white,
        )
      ],
    ).show();
  }
  var alertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: false,
    descStyle: TextStyle(fontWeight: FontWeight.bold),
    animationDuration: Duration(milliseconds: 400),
    alertBorder: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(0.0),
      side: BorderSide(
        color: Colors.grey,
      ),
    ),
    titleStyle: TextStyle(
      color: Colors.white,
    ),
  );
  Completer<WebViewController> _controller = Completer<WebViewController>();
  DateTime selectedDate;
  DateTime selectedDates;
  TextEditingController emailController = TextEditingController();
  TextEditingController marqueController = TextEditingController();
  TextEditingController immatriculationController = TextEditingController();
  TextEditingController valeur_neuve_Controller = TextEditingController();
  TextEditingController venaleController = TextEditingController();


  int _currentStep = 0;
SingingCharacter _character = SingingCharacter.un_ans;
SingingCharacters _characters = SingingCharacters.auto;
SingingCharacters_model _characters_model = SingingCharacters_model.auto_break;
SingingCharacters_ _characters_ = SingingCharacters_.essence;
SingingCharacters_categorie _characters_categorie = SingingCharacters_categorie.prope_compte;

  session_suivant(date_permis,dure_assurance,type_vehicule,model,categorie,marque,immatriculation,valeur_neuve,valeur_venale,energie) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('date permis', date_permis);
    prefs.setString('dure assu', dure_assurance);
    prefs.setString('type vehicule', type_vehicule);
    prefs.setString('model', model);
    prefs.setString('categorie', categorie);
    prefs.setString('marque', marque);
    prefs.setString('valeur_neuve', valeur_neuve);
    prefs.setString('valeur_venale', valeur_venale);
    prefs.setString('energie', energie);
    prefs.setString('immatriculation', immatriculation);
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: AppBar(
          title: Text('Demande De Cotation'),
        ),
        body: Column(children: <Widget>[
          Expanded(
            child: Stepper(
              currentStep: _currentStep,
              onStepContinue: () {
                if (_currentStep >= 9){

/*                 if(selectedDate.toString().isEmpty){
                    print("date absent 1 vide");
                    _Alert_controller(context);
                  }else if(selectedDates.toString().isEmpty){
                    _Alert_controller(context);
                    print("date absent 2 vide ");
                  }else if(_characters.toString().contains("null")){
                    _Alert_controller(context);
                    print("temps assurance vide");
                  }else if(_character.toString().contains("null")){
                    _Alert_controller(context);
                    print("auto_moto vide");
                  }else if(_characters_model.toString().contains("null")){
                    _Alert_controller(context);
                    print("model vide");
                  }else if(_characters_categorie.toString().contains("null")){
                    _Alert_controller(context);
                    print("categorie vide");
                  }else if(marqueController.text ==""){
                    _Alert_controller(context);
                    print("marque vide");
                  }else if(immatriculationController.text ==""){
                    _Alert_controller(context);
                    print("imatriculation vide");
                  }else if(valeur_neuve_Controller.text ==""){
                    _Alert_controller(context);
                    print("valeur neuve vide");
                  }else if(venaleController.text ==""){
                    _Alert_controller(context);
                    print("valeur venale vide");
                  }else if(_characters_.toString().contains("null")){
                    _Alert_controller(context);
                    print("modele essence ou gasoile");
                  }else{*/
                   // var cacs_model_essence = _characters_.toString().split(".");
                   // print(cacs_model_essence[1]);
                    Navigator.push(
                        context,
                        PageTransition(
                            curve: Curves.linear,
                            type: PageTransitionType.rotate,duration: Duration(seconds: 1),
                            child: Home_two()));
                 // }

                  return;
                }
                setState(() {
                  _currentStep += 1;
                });

              },
              onStepCancel: () {
                if (_currentStep <= 0) return;
                setState(() {
                  _currentStep -= 1;
                });
              },
              steps:  <Step>[
                Step(
                  title: Text('Type de vehicule'),
                  content: SizedBox(
                    width: 300.0,
                    height: 112.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('auto'),
                          leading: Radio(
                            value: SingingCharacters.auto,
                            groupValue: _characters,
                            onChanged: (SingingCharacters value) {
                              setState(() { _characters = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('moto'),
                          leading: Radio(
                            value: SingingCharacters.moto,
                            groupValue: _characters,
                            onChanged: (SingingCharacters value) {
                              setState(() { _characters = value; });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Step(
                  title: Text('Date d\'obtention du permis'),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,

                      child: DateTimeField(
                        selectedDate: selectedDate,label: "Date d'obtention du permis",
                        mode:DateFieldPickerMode.date,lastDate:DateTime(2021),
                        onDateSelected: (DateTime date) {
                          setState(() {
                             selectedDate =  date;
                          });
                        },
                      ),
                  ),
                ),
                Step(
                  title: Text('debut du contrat'),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,
                      child:DateTimeField(
                        selectedDate:selectedDates,label: "debut du contrat",
                        mode:DateFieldPickerMode.date,lastDate:DateTime(2021),
                        onDateSelected: (DateTime date) {
                          setState(() {
                            selectedDates =  date;
                          });
                        },
                      ),
                  ),
                ),
                Step(
                  title: Text('duré assurances'),
                  content: SizedBox(
                      width: 300.0,
                      height: 224.0,
                      child:Column(
                        children: <Widget>[
                          ListTile(
                            title: const Text('1 an'),
                            leading: Radio(
                              value: SingingCharacter.un_ans,
                              groupValue: _character,
                              onChanged: (SingingCharacter value) {
                                setState(() { _character = value; });
                              },
                            ),
                          ),
                          ListTile(
                            title: const Text('6 mois'),
                            leading: Radio(
                              value: SingingCharacter.six_mois,
                              groupValue: _character,
                              onChanged: (SingingCharacter value) {
                                setState(() { _character = value; });
                              },
                            ),
                          ),ListTile(
                            title: const Text('3 mois'),
                            leading: Radio(
                              value: SingingCharacter.trois_mois,
                              groupValue: _character,
                              onChanged: (SingingCharacter value) {
                                setState(() { _character = value; });
                              },
                            ),
                          ),ListTile(
                            title: const Text('1 mois'),
                            leading: Radio(
                              value: SingingCharacter.un_mois,
                              groupValue: _character,
                              onChanged: (SingingCharacter value) {
                                setState(() { _character = value;
                                });
                              },
                            ),
                          ),
                        ],
                      ),
                    /*TextField(
                    //controller: emailController,

                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'email',

                      ))*/
                  ),
                ),
                Step(
                  title: Text('Marque de vehicule '),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,
                      child:TextField(
                    controller: marqueController,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Marque de vehicule',

                      ))
                  ),
                ),
                Step(
                  title: Text('modele de vehicule'),
                  content: SizedBox(
                    width: 300.0,
                    height: 393.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('auto Break'),
                          leading: Radio(
                            value: SingingCharacters_model.auto_break,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('monospace'),
                          leading: Radio(
                            value: SingingCharacters_model.monospace,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value; });
                            },
                          ),
                        ),ListTile(
                          title: const Text('citadines'),
                          leading: Radio(
                            value: SingingCharacters_model.citadine,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value; });
                            },
                          ),
                        ),ListTile(
                          title: const Text('cabriolets'),
                          leading: Radio(
                            value: SingingCharacters_model.cabriole,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('Pick up'),
                          leading: Radio(
                            value: SingingCharacters_model.pic_up,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('4x4'),
                          leading: Radio(
                            value: SingingCharacters_model.quatre_quartre,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('crossovers'),
                          leading: Radio(
                            value: SingingCharacters_model.crossovers,
                            groupValue: _characters_model,
                            onChanged: (SingingCharacters_model value) {
                              setState(() { _characters_model = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),Step(
                  title: Text('immatriculation'),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,
                      child:TextField(
                        controller: immatriculationController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'immatriculation',
                          ))
                  ),
                ),Step(
                  title: Text('valeur neuve'),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,
                      child:TextField(
                        controller: valeur_neuve_Controller,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'valeur neuve',

                          ))
                  ),
                ),Step(
                  title: Text('energie'),
                  content: SizedBox(
                    width: 300.0,
                    height: 112.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('essence'),
                          leading: Radio(
                            value: SingingCharacters_.essence,
                            groupValue: _characters_,
                            onChanged: (SingingCharacters_ value) {
                              setState(() { _characters_ = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('diesel'),
                          leading: Radio(
                            value: SingingCharacters_.gasoil,
                            groupValue: _characters_,
                            onChanged: (SingingCharacters_ value) {
                              setState(() { _characters_ = value; });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),Step(
                  title: Text('valeur venale'),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,
                      child:TextField(
                        controller: venaleController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'valeur venale',
                          ))
                  ),
                ),

              ],

            ),
          ),
        ]));

  }

}
  _markertplaceUrl() async{
    const url = 'http://192.168.1.9/demo/';
    if(await canLaunch(url)){

      await launch(url);
    }else{
      throw 'Could not launch $url';
    }
  }

