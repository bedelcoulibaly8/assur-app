import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'montant.dart';

class Accueil extends StatefulWidget {
  static List<String> images = ["assets/images/orange_money.jpg",
    "assets/images/mtn_money.jpg",
    "assets/images/moov-money.png",
    "assets/images/visa.png",
    "assets/images/master_card.png"];

  @override
  _AccueilState createState() => _AccueilState();
}

class _AccueilState extends State<Accueil> {

  bool _validateMontant = false;
  String montant_error_text = null;


  @override
  Widget build(BuildContext context) {

    return Material(

/*      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,*/
      color: Color(0xFFFFFFFF),
      child: Column(
        children: <Widget>[
          Expanded(
              flex: 1,
              child: Container(

                padding: EdgeInsets.all(10),
                child: Center(
                  child: Text(
                    "Choisissez un moyen\nde paiement",
                    style: TextStyle(fontSize: 25.0, color:Color(0xFF000000),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              )

          ),

          Expanded(
            flex: 2,
              child: GridView.builder(
// Create a grid with 2 columns. If you change the scrollDirection to
// horizontal, this produces 2 rows.
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3, mainAxisSpacing: 0
                  ),
                  padding: const EdgeInsets.all(5),
                  itemCount: Accueil.images.length,
                  itemBuilder: (context, index) {
                    return Container(
                        padding: EdgeInsets.all(0),
                        margin: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: ConstrainedBox(
                            constraints: BoxConstraints.expand(),
                            child:FlatButton(
                              color: Colors.white,
                              child: Image.asset(Accueil.images[index]),
                              onPressed: () {
                                if (index == 0) {
                                  _showDialog(context, "orange");
                                } else if (index == 2) {
                                  _showDialog(context, "moov");
                                } else if (index == 3 || index == 4) {
                                  _showDialog(context, "visa");
                                }
                              },
                            )
                        )
                    );

                  }
// Generate 100 widgets that display their index in the List.

              )

          )


        ],
      ),
    );
  }

  void _showDialog(BuildContext context, String operateur) {
    // flutter defined function
    TextEditingController _controllerMontant = new TextEditingController();

    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Entrer le montant"),
          content: new TextField(
            controller: _controllerMontant,
            decoration: InputDecoration(
              hintText: "Montant",
              errorText: _validateMontant ? null : montant_error_text,
            ),
            keyboardType: TextInputType.number,
            onChanged: (value) {
              if(value.isNotEmpty){
                setState(() {
                  _validateMontant = true;
                  montant_error_text = null;
                });
              }else if(int.parse(value) >= 100){
                setState(() {
                  _validateMontant = true;
                  montant_error_text = null;
                });
              }else{
                setState(() {
                  _validateMontant = false;
                  montant_error_text = "Montant invalide";
                });
              }
            },
          ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Valider"),
              onPressed: () {
                if(int.parse(_controllerMontant.text) < 100){
                  _validateMontant = false;
                  montant_error_text = "Montant invalide";
                }else{
                  _validateMontant = true;
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => Montant(_controllerMontant.text, operateur)));
                }
              },
            ),
            new FlatButton(
              child: new Text("Annuler"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void valMontant(int montant, String text, bool val){
    if(montant < 100){
      text = "Montant invalide";
      val = false;
    }else{
      val = true;
      text = null;
    }
  }


}

//http.post(url,
//            body: json.encode(body),
//            headers: { 'Content-type': 'application/json',

//              'Accept': 'application/json',
//              "Authorization": "Some token"},
//            encoding: encoding)"""


