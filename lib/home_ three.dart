import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:webview_flutter/webview_flutter.dart';
import'package:steppers/steppers.dart ';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:date_field/date_field.dart';
import 'package:gnaassurance/home_two.dart';

import 'accueil.dart';





class Home_three extends StatefulWidget {
  @override
  _Home_threeState createState() => new _Home_threeState();
}
enum SingingCharacters { homme,femme }

class _Home_threeState extends State<Home_three> {
  Completer<WebViewController> _controller = Completer<WebViewController>();
  TextEditingController name_controller = TextEditingController();
  TextEditingController prenom_controller = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController numero_Controller = TextEditingController();


  SingingCharacters _character = SingingCharacters.homme;
  DateTime selectedDate;

  int _currentStep = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Souscription'),
        ),
        body: Column(children: <Widget>[
          Expanded(
            child: Stepper(
              currentStep: _currentStep,
              onStepContinue: () {
                if (_currentStep >= 5){
                  print('go at other step');
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Accueil()),
                    );
                  return;
                }
                setState(() {
                  _currentStep += 1;
                });

              },
              onStepCancel: () {
                if (_currentStep <= 0) return;
                setState(() {
                  _currentStep -= 1;
                });
              },
              steps:  <Step>[
                Step(
                  title: Text('Genre'),
                  content: SizedBox(
                    width: 300.0,
                    height: 112.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('Homme'),
                          leading: Radio(
                            value: SingingCharacters.homme,
                            groupValue: _character,
                            onChanged: (SingingCharacters value) {
                              setState(() { _character = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Femme'),
                          leading: Radio(
                            value: SingingCharacters.femme,
                            groupValue: _character,
                            onChanged: (SingingCharacters value) {
                              setState(() { _character = value; });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Step(
                  title: Text('Date de naissance'),
                  content: SizedBox(
                    width: 300.0,
                    height: 49.0,
                    child: DateTimeField(
                      selectedDate: selectedDate,label: "Date de naissance",
                      mode:DateFieldPickerMode.date,lastDate:DateTime(2021),
                      onDateSelected: (DateTime date) {
                        setState(() {
                          selectedDate =  date;
                        });
                      },
                    ),
                  ),
                ),
                Step(
                  title: Text('Nom'),
                  content: SizedBox(
                      width: 300.0,
                      height: 49.0,
                      child:TextField(
                          controller: name_controller,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'nom',
                          ))
                  ),
                ),
                Step(
                  title: Text('Prenom'),
                  content: SizedBox(
                      width: 300.0,
                      height: 49.0,
                      child:TextField(
                          controller: prenom_controller,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Prenom',
                          ))
                  ),
                ),
                Step(
                  title: Text('Numero'),
                  content: SizedBox(
                      width: 300.0,
                      height: 49.0,
                      child:TextField(
                        controller: numero_Controller,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Numero',
                          ))
                  ),
                ), Step(
                  title: Text('Email'),
                  content: SizedBox(
                      width: 300.0,
                      height: 49.0,
                      child:TextField(
                        controller: emailController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'email',
                          ))
                  ),
                ),

              ],

            ),
          ),
        ]));

  }

}
_markertplaceUrl() async{
  const url = 'http://192.168.1.9/demo/';
  if(await canLaunch(url)){

    await launch(url);
  }else{
    throw 'Could not launch $url';
  }
}

