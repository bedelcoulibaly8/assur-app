import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:webview_flutter/webview_flutter.dart';
import 'dart:convert';
import 'dart:math';

class Montant extends StatefulWidget {

  final String montant, operateur;

  const Montant(this.montant, this.operateur);

  @override
  _MontantState createState() => _MontantState();
}

class _MontantState extends State<Montant> {
  String body, url="";

  static var _id = new Random();

  int order_id = _id.nextInt(100000);

  /*
  curl --location --request POST 'https://api.bizao.com/mobilemoney/v1' \
  --header 'Authorization: Bearer 989c84ef-5d6e-31fa-9964-e00f64d9de0e' \
  --header 'country-code: ci' \
  --header 'mno-name: orange' \
  --header 'lang: fr' \
  --header 'channel: web' \
  --header 'Content-Type: application/json' \
  --header 'Cookie: SERVERID=s0' \
  --data-raw '{
  "currency": "XOF",
  "order_id": "Sacha-BddsfsSC-Test-02",
  "amount": 1,
  "state": "testForBSC",
  "return_url": "https://callback.bizao.com/MM/return.html",
  "cancel_url": "https://callback.bizao.com/MM/cancel.html",
  "notif_url": "https://callback.bizao.com/MM/save.php",
  "reference": "MM-Bizao-acceptance"
  }'
  * */

  _makeMobilePostRequest(String montant, String operateur) async {
    // set up POST request arguments
    String url = 'https://api.bizao.com/mobilemoney/v1';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "Authorization":"Bearer dcd668df-711b-3f43-80bd-9fe693f5b956",
      "country-code":"ci",
      "mno-name":operateur,
      "Cookie":"SERVERID=s0",
      "lang":"fr",
      "channel":"web"
    };
    String jsons = '{"currency": "XOF","order_id": '+order_id.toString()+',"amount":'+montant+',"return_url": "https://callback.bizao.com/MM/return.html", "cancel_url": "https://callback.bizao.com/MM/cancel.html","notif_url": "https://callback.bizao.com/MM/save.html","user_msisdn": "22551428818", "reference": "MM-Bizao-acceptance", "state": "testForBSC"}';
    // make POST request
    final response = await http.post(url, headers: headers, body: jsons);
    // check the status code for the result
    int statusCode = response.statusCode;
    print(statusCode);

    if(statusCode == 201) {
      var content;
      setState(() {
        body = response.body;
        print(body);
        if(widget.operateur == "moov"){
          content = json.decode(body);
          body = content['payment_url'];
        }else{
          body = body.split(":")[4] + ":" + body.split(":")[5].split(",")[0];
          body = body.replaceAll("\"", "");
        }

      });
    }else
      print(body);
  }



  _makeVisaPostRequest(String montant) async {
    // set up POST request arguments
    String url = 'https://pre-gateway.bizao.com/debitCard/v1';
    Map<String, String> headers = {
      "Content-type": "application/json",
      "Authorization":"Bearer c38b4547-8599-3b19-9982-6104148ecaa4",
      "category":"education",
      "country-code":"ci",
      "lang":"en",
      "Cookie":"SERVERID=s0"
    };
    String jsons = '{"currency": "XOF","order_id": '+order_id.toString()+',"amount":'+montant+',"return_url": "https://callback.bizao.com/MM/return.html", "reference": "abc000", "state":"test000"}';
    // make POST request
    final response = await http.post(url, headers: headers, body: jsons);
    // check the status code for the result
    int statusCode = response.statusCode;
    print(statusCode);

    if(statusCode == 201) {
      setState(() {
        body = response.body;
        //body = body.split(":")[5] + ":" + body.split(":")[6];
        //body = body.replaceAll("\"", "");
        //body = body.replaceAll("}", "");
      });
    }

  }

  @override
  void initState() {
    if(widget.operateur == "visa")
      _makeVisaPostRequest(widget.montant);
    else
      _makeMobilePostRequest(widget.montant, widget.operateur);
    super.initState();
  }

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    CircularProgressIndicator();
    if(body == null){
      return Scaffold(
        appBar: AppBar(
          title: Text("Paiement"),
        ),
        body: Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.white,
          ),
        ),
      );
    }else{
      return Scaffold(
        appBar: AppBar(
          title: Text("Paiement"),
        ),
        body: Builder(builder: (BuildContext context) {
            return WebView(
              initialUrl: body,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller.complete(webViewController);
              },
            );
          }
        )
      );
    }
  }
}
