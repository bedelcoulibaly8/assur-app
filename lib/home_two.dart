import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gnaassurance/home_one.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'package:gnaassurance/home_ three.dart';
import'package:steppers/steppers.dart ';
import 'package:backdrop_modal_route/backdrop_modal_route.dart';

class Home_two extends StatefulWidget {
  @override
  _Home_twoState createState() => new _Home_twoState();
}
enum SingingCharacters_categorie { tourisme, prope_compte}
enum SingingCharacters_sociaux { salarier, sans_emploie,artisan,retraiter,femme_foyer,agent_commercial,exploitant_agricole,religieux}
enum SingingCharacters_couverture{ Tiers_simple,Tiers_complet,exploitants,Tiers_collision,tout_risque}
enum SingingCharacters_Assistance{oui,non}

class _Home_twoState extends State<Home_two> {
  //_Home_twoState({Key key}) : super(key: key);

  Completer<WebViewController> _controller = Completer<WebViewController>();
  SingingCharacters_categorie _characters_categorie = SingingCharacters_categorie.prope_compte;
  SingingCharacters_sociaux _characters_sociaux = SingingCharacters_sociaux.agent_commercial;
  SingingCharacters_couverture characters_couverture = SingingCharacters_couverture.exploitants;
  SingingCharacters_Assistance _characters_assistance = SingingCharacters_Assistance.oui;
  TextEditingController puissanceController = TextEditingController();

  int _currentStep = 0;
  String backdropResult = '';

  void handleCustomizedBackdropContent(BuildContext context) async {
    await Navigator.push(
      context,
      BackdropModalRoute<void>(
        overlayContentBuilder: (context) {
          return Container(
            height: MediaQuery.of(context).size.height - 1.0,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(50),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('	Responsabilité Civile:   1'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('	Défense et Recours :   1000'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('	Dommages Tous Accident:  1234'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Text('  Vol & Agression      :   5000'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('Incendie :   3000'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('Nombre de passagé     :   6000'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('Dommages Tous Accident  :  3000'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('Bris De Glace  :  3500'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('Avance sur Recours  :  3700'),
                ),Divider(),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text('A payer  :  500000'),
                ),Divider(),

                RaisedButton(
                    onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Home_three()),
                       );
                         },
                  child: Text('Souscription'),
                ),
              ],
            ),
          );
        },
        topPadding: 1.0,
        barrierColorVal: Colors.black,
        backgroundColor: Colors.white30,
        backdropShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(60),
            topRight: Radius.circular(60),
            bottomLeft: Radius.circular(50),
            bottomRight: Radius.circular(50),
          ),
        ),
        barrierLabelVal: 'Customized Backdrop',
        shouldMaintainState: false,
        canBarrierDismiss: false,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Demande De Cotation'),
        ),
        body: Column(children: <Widget>[
          Expanded(
            child: Stepper(
              currentStep: _currentStep,
              onStepContinue: () {
                  if (_currentStep >= 4){
                    handleCustomizedBackdropContent(context);
                    print('fonction to do a cotation');
                   // _showLoading(context);
    /*                  Navigator.push(
                      context,
                      PageTransition(
                          curve: Curves.linear,
                          type: PageTransitionType.rotate,duration: Duration(seconds: 1),
                          child: Home_three()));*/
                  return;
                }
                setState(() {
                  //verification categorie
                  if(_characters_categorie.toString().contains("null")){
                    print("categorie vide");
                  }else{
                    var cacs_cate = _characters_categorie.toString().split(".");
                    print(cacs_cate[1]);
                  }
                  //verification categorie sociaux
                  if(_characters_sociaux.toString().contains("null")){
                    print("categorie sociaux vide");
                  }else{
                    var cacs_cate = _characters_sociaux.toString().split(".");
                    print(cacs_cate[1]);
                  }
                  //verification couverture
                  if(characters_couverture.toString().contains("null")){
                    print("categorie couverture vide");
                  }else{
                    var cacs_cates = characters_couverture.toString().split(".");
                    print(cacs_cates[1]);
                  }
                  //verification assistance
                  if(_characters_assistance.toString().contains("null")){
                    print("categorie couverture vide");
                  }else{
                    var cacss_cates = _characters_assistance.toString().split(".");
                    print(cacss_cates[1]);
                  }
                  //verification puissance
                  if(puissanceController.text ==""){
                    print("puissance vide");
                  }else{
                    print(puissanceController.text);
                  }
                  _currentStep += 1;
                });



              },
              onStepCancel: () {
                if (_currentStep <= 0) return;
                setState(() {
                  _currentStep -= 1;
                });
              },
              steps:  <Step>[
                Step(
                  title: Text('Catégorie'),
                    content: SizedBox(
                      width: 300.0,
                      height: 112.0,
                      child:Column(
                        children: <Widget>[
                          ListTile(
                            title: const Text('tourisme et affaire'),
                            leading: Radio(
                              value: SingingCharacters_categorie.tourisme,
                              groupValue: _characters_categorie,
                              onChanged: (SingingCharacters_categorie value) {
                                setState(() { _characters_categorie = value; });
                              },
                            ),
                          ),
                          ListTile(
                            title: const Text('propre et compte privé'),
                            leading: Radio(
                              value: SingingCharacters_categorie.prope_compte,
                              groupValue: _characters_categorie,
                              onChanged: (SingingCharacters_categorie value) {
                                setState(() { _characters_categorie = value; });
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                ),
                Step(
                  title: Text('puissance fiscale'),
                  content: SizedBox(
                      width: 300.0,
                      height: 50.0,
                      child:TextField(
                        controller: puissanceController,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'puissance fiscale ',
                          ))
                  ),
                ),
                Step(
                  title: Text('Catégorie Sociaux professionnelle'),
                  content: SizedBox(
                    width: 300.0,
                    height: 393.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('salarié'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.salarier,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('sans emploie'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.sans_emploie,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux = value; });
                            },
                          ),
                        ),ListTile(
                          title: const Text('religieux'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.religieux,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux = value; });
                            },
                          ),
                        ),ListTile(
                          title: const Text('artisan'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.artisan,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux= value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('exploitant'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.exploitant_agricole,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux = value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('retraiter'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.retraiter,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux = value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('agent commercial'),
                          leading: Radio(
                            value: SingingCharacters_sociaux.retraiter,
                            groupValue: _characters_sociaux,
                            onChanged: (SingingCharacters_sociaux value) {
                              setState(() { _characters_sociaux = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Step(
                  title: Text('couverture'),
                  content: SizedBox(
                    width: 300.0,
                    height: 280.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('Tiers simple'),
                          leading: Radio(
                            value: SingingCharacters_couverture.Tiers_simple,
                            groupValue: characters_couverture,
                            onChanged: (SingingCharacters_couverture value) {
                              setState(() { characters_couverture = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('Tiers complet'),
                          leading: Radio(
                            value: SingingCharacters_couverture.Tiers_complet,
                            groupValue: characters_couverture,
                            onChanged: (SingingCharacters_couverture value) {
                              setState(() { characters_couverture = value; });
                            },
                          ),
                        ),ListTile(
                          title: const Text('Tiers collision'),
                          leading: Radio(
                            value: SingingCharacters_couverture.Tiers_collision,
                            groupValue: characters_couverture,
                            onChanged: (SingingCharacters_couverture value) {
                              setState(() { characters_couverture = value; });
                            },
                          ),
                        ),ListTile(
                          title: const Text('Tout risque'),
                          leading: Radio(
                            value: SingingCharacters_couverture.tout_risque,
                            groupValue: characters_couverture,
                            onChanged: (SingingCharacters_couverture value) {
                              setState(() { characters_couverture = value;
                              });
                            },
                          ),
                        ),ListTile(
                          title: const Text('exploitant'),
                          leading: Radio(
                            value: SingingCharacters_couverture.exploitants,
                            groupValue: characters_couverture,
                            onChanged: (SingingCharacters_couverture value) {
                              setState(() { characters_couverture = value;
                              });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Step(
                  title: Text('garantie Assistance Routière'),
                  content: SizedBox(
                    width: 300.0,
                    height: 112.0,
                    child:Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('oui'),
                          leading: Radio(
                            value: SingingCharacters_Assistance.oui,
                            groupValue: _characters_assistance,
                            onChanged: (SingingCharacters_Assistance value) {
                              setState(() { _characters_assistance = value; });
                            },
                          ),
                        ),
                        ListTile(
                          title: const Text('non'),
                          leading: Radio(
                            value: SingingCharacters_Assistance.non,
                            groupValue: _characters_assistance,
                            onChanged: (SingingCharacters_Assistance value) {
                              setState(() { _characters_assistance = value; });
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],

            ),
          ),
        ]));

  }

}
_markertplaceUrl() async{
  const url = 'http://192.168.1.9/demo/';
  if(await canLaunch(url)){

    await launch(url);
  }else{
    throw 'Could not launch $url';
  }
}

