
import 'package:flutter/material.dart';
import 'package:gnaassurance/accueil.dart';
import './home_one.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Assurance',
      debugShowCheckedModeBanner: false,

      theme: ThemeData(primaryColor: Color(0xffe2b80e),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:Home(),
    );
  }
}